# Android Views

[![Download](https://api.bintray.com/packages/wylhyz/maven/android-views/images/download.svg)](https://bintray.com/wylhyz/maven/android-views/_latestVersion)
[![Build Status](https://travis-ci.org/wylhyz/AndroidViews.svg?branch=master)](https://travis-ci.org/wylhyz/AndroidViews)

一些常用的views和widgets的集合

使用方法

project -> build.gradle
```gradle
allprojects {
    repositories {
        jcenter()

        maven {
            url 'https://dl.bintray.com/wylhyz/maven'
        }
    }
}
```

然后添加下面到项目module的build.gradle下
```gradle
compile 'io.lhyz.android:views:0.0.3'
```